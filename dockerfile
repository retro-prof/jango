FROM python:3-alpine

WORKDIR /docker-cicd-django
COPY requirements.txt .
COPY . .
RUN pip install -r requirements.txt && \
    mkdir db && \
    sh init.sh 

EXPOSE 8080

CMD python manage.py runserver 0.0.0.0:8080

# RUN python3 -m venv myvenv && \
#     source myvenv/bin/activate && \
#     python3 -m pip install --upgrade pip

# RUN python manage.py migrate blog && \
#     python manage.py createsuperuser



